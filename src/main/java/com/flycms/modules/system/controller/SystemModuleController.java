package com.flycms.modules.system.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.common.validator.Sort;
import com.flycms.modules.system.entity.SystemModule;
import com.flycms.modules.system.service.SystemModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 2:11 2019/8/25
 */
@Controller
@RequestMapping("/admin/module")
public class SystemModuleController extends BaseController {
    @Autowired
    private SystemModuleService systemModuleService;


    @GetMapping("/add{url.suffix}")
    public String add()
    {
        return"system/admin/module/add";
    }

    @GetMapping("/list{url.suffix}")
    public String siteList()
    {
        return "system/admin/module/list";
    }

    @GetMapping("/listData{url.suffix}")
    @ResponseBody
    public Object siteList(SystemModule module,
                           @RequestParam(value = "page",defaultValue = "1") Integer page,
                           @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize,
                           @Sort(accepts = {"add_time", "id"}) @RequestParam(defaultValue = "add_time") String sort,
                           @RequestParam(defaultValue = "desc") String order) {
        return systemModuleService.selectModulePager(module, page, pageSize, sort, order);
    }
}
