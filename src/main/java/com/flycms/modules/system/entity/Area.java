package com.flycms.modules.system.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 区域地址实体类
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 15:11 2019/8/29
 */
@Setter
@Getter
public class Area implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private Integer parentId;
    private String areaName;
    private Integer type;
    private Integer code;
    private Integer sort;
}
