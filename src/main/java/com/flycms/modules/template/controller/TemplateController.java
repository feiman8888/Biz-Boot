package com.flycms.modules.template.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.common.exception.ApiAssert;
import com.flycms.common.pager.Pager;
import com.flycms.common.validator.Sort;
import com.flycms.modules.system.service.SystemModuleService;
import com.flycms.modules.template.entity.*;
import com.flycms.modules.template.service.TemplateColumnService;
import com.flycms.modules.template.service.TemplatePageService;
import com.flycms.modules.template.service.TemplateService;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 模版相关操作
 *
 * @author 孙开飞
 */
@Controller
@RequestMapping("/admin/template")
public class TemplateController extends BaseController {

	@Autowired
	private TemplatePageService templatePageService;

	@Autowired
	private TemplateService templateService;

	@Autowired
	private TemplateColumnService templateColumnService;

	@Autowired
	private SystemModuleService systemModuleService;

	@GetMapping("/add{url.suffix}")
	public String add(Model model)
	{
		List<TemplateColumn> column=templateColumnService.selectTemplateColumnList();
		model.addAttribute("column",column);
		return"system/admin/template/add";
	}

	@PostMapping("/add{url.suffix}")
	@ResponseBody
	public Object add(Template template)
	{
		return templateService.addDeveloperTemplate(template);
	}

	@GetMapping("/edit{url.suffix}")
	public String edit(@RequestParam(value = "id", required = false) String id,Model model)
	{
		ApiAssert.notTrue(StringUtils.isEmpty(id) , "模板id不能为空");
		ApiAssert.notTrue(!NumberUtils.isDigits(id), "模板id参数错误");
		Template tp=templateService.findById(Long.parseLong(id));
		List<TemplateColumn> column=templateColumnService.selectTemplateColumnList();
		model.addAttribute("column",column);
		model.addAttribute("id",id);
		model.addAttribute("tp",tp);
		return"system/admin/template/edit";
	}

	@PostMapping("/edit{url.suffix}")
	@ResponseBody
	public Object edit(Template template)
	{
		return templateService.updateDeveloperTemplatePage(template);
	}

	@GetMapping("/list{url.suffix}")
	public Object templateList(Template template,
							   @RequestParam(value = "page",defaultValue = "1") Integer page,
							   @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize,
							   @Sort(accepts = {"add_time", "id"}) @RequestParam(defaultValue = "add_time") String sort,
							   @RequestParam(defaultValue = "desc") String order,Model model) {
		Pager<Template> pager=templateService.selectTemplatePager(template, page, pageSize, sort, order);
		model.addAttribute("pager",pager);
		return "system/admin/template/list";
	}

	/**
	 * 添加模板页面
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/page/add{url.suffix}")
	public String addTemplatePage(@RequestParam(value = "id", required = false) String id,Model model){
		ApiAssert.notTrue(StringUtils.isEmpty(id) , "模板id不能为空");
		ApiAssert.notTrue(!NumberUtils.isDigits(id), "模板id参数错误");
		List<TemplateVO> templatelist = templateService.selectTemplateList();
		//模型列表
		model.addAttribute("modulelist",systemModuleService.selectModuleList());
		model.addAttribute("templatelist",templatelist);
		model.addAttribute("templateId",id);
		return "system/admin/template/page/add";
	}

	@PostMapping("/page/add{url.suffix}")
	@ResponseBody
	public Object addTemplatePage(TemplatePage templatePage)
	{
		return templatePageService.addDeveloperTemplatePage(templatePage);
	}

	/**
	 * 添加模板页面
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/page/edit{url.suffix}")
	public String editTemplatePage(@RequestParam(value = "id", required = false) String id,Model model){
		ApiAssert.notTrue(StringUtils.isEmpty(id) , "模板id不能为空");
		ApiAssert.notTrue(!NumberUtils.isDigits(id), "模板id参数错误");
		TemplatePage template=templatePageService.findById(Long.parseLong(id));
		//转义html代码
		if(template.getTemplatePage()!=null){
			template.setTemplatePage(HtmlUtils.htmlEscape(template.getTemplatePage()));
		}
		ApiAssert.notNull(template , "模板不存在");
		List<TemplateVO> templatelist = templateService.selectTemplateList();
		//模型列表
		model.addAttribute("modulelist",systemModuleService.selectModuleList());
		model.addAttribute("templatelist",templatelist);
		model.addAttribute("templateId",id);
		model.addAttribute("template",template);
		return "system/admin/template/page/edit";
	}

	@PostMapping("/page/edit{url.suffix}")
	@ResponseBody
	public Object editTemplatePage(TemplatePage templatePage)
	{
		return templatePageService.updateDeveloperTemplatePage(templatePage);
	}


	@GetMapping("/page/list{url.suffix}")
	public String templatePageList(@RequestParam(value = "id", required = false) String id,Model model)
	{
		ApiAssert.notTrue(StringUtils.isEmpty(id) , "模板id不能为空");
		ApiAssert.notTrue(!NumberUtils.isDigits(id), "模板id参数错误");
		model.addAttribute("id",id);
		return "system/admin/template/page/list";
	}

	@GetMapping("/page/listData{url.suffix}")
	@ResponseBody
	public Object templatePageList(TemplatePageVO templatePageVO,
						   @RequestParam(value = "page",defaultValue = "1") Integer page,
						   @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize,
						   @Sort(accepts = {"add_time", "id"}) @RequestParam(defaultValue = "add_time") String sort,
						   @RequestParam(defaultValue = "desc") String order) {
		ApiAssert.notTrue(StringUtils.isEmpty(templatePageVO.getTemplateId()) , "模板id不能为空");
		ApiAssert.notTrue(!NumberUtils.isDigits(templatePageVO.getTemplateId()), "模板id参数错误");
		//javabean 映射工具
		MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		MapperFacade mapper = mapperFactory.getMapperFacade();
		TemplatePage templatePage = mapper.map(templatePageVO,TemplatePage.class);
		return templatePageService.selectTemplatePagePager(templatePage, page, pageSize, sort, order);
	}
}