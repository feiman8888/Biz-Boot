package com.flycms.modules.help.service;

import com.flycms.modules.help.entity.Help;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @Description: 帮助服务接口
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:10 2019/8/17
 */
public interface HelpService {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    /**
     *
     *
     * @param help
     * @return
     */
    public Object addHelp(Help help);
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 按id删除帮助信息
     *
     * @param id
     * @return
     */
    public int deleteById(Long id);
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    /**
     * 更新帮助信息
     *
     * @param help
     * @return
     */
    public Object updateHelp(Help help);


    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 按网站id查询标题是否存在
     *
     * @param siteId
     *         网站id
     * @param title
     *         帮助信息标题
     * @param id
     *         需要排除id
     * @return
     */
    public boolean checkHelpByTitle(Long siteId, String title, Long id);

    /**
     * 按id查询友情链接
     *
     * @param id
     * @return
     */
    public Help findById(Long id);

    /**
     * 查询网站所属帮助信息
     *
     * @param siteid
     *         所输网站id
     * @param title
     *         帮助内容标题
     * @param page
     *         当前页数
     * @param pageSize
     *         每页显示数量
     * @param sort
     *         指定排序字段
     * @param order
     *         排序方式
     * @return
     */
    public Object queryHelpPager(Long siteid, String title, Integer page, Integer pageSize, String sort, String order);
}
