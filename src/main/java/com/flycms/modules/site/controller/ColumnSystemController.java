package com.flycms.modules.site.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.common.exception.ApiAssert;
import com.flycms.common.utils.result.LayResult;
import com.flycms.common.utils.result.Result;
import com.flycms.common.validator.Sort;
import com.flycms.modules.site.entity.SiteColumn;
import com.flycms.modules.site.entity.SiteColumnBO;
import com.flycms.modules.system.service.SystemModuleService;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 公共的
 * @author 孙开飞
 */
@Controller
@RequestMapping("/admin/system_column")
public class ColumnSystemController extends BaseController {
    @Autowired
    private SystemModuleService systemModuleService;

    @GetMapping("/add{url.suffix}")
    public Object add(@RequestParam(value = "siteId", required = false) String siteId,
                      @RequestParam(value = "parentId", defaultValue = "0") String parentId, Model model){
        ApiAssert.notTrue(siteId == null || !NumberUtils.isNumber(siteId), "网站id为空或者类型错误");

        //父级分类
        if(!StringUtils.isEmpty(siteId) && (!StringUtils.isEmpty(parentId) && Long.parseLong(parentId) > 0l)){
            String parentName=siteColumnService.parentName(Long.parseLong(parentId),Long.parseLong(siteId));
            model.addAttribute("parentName",parentName);
        }
        model.addAttribute("parentId",parentId);
        //模型列表
        model.addAttribute("modulelist",systemModuleService.selectModuleList());
        //网站id
        model.addAttribute("siteId",siteId);
        return "system/admin/system/site/column/add";
    }


    @PostMapping("/add{url.suffix}")
    @ResponseBody
    public Object add(@Valid SiteColumnBO siteColumnBO, BindingResult bindingResult)
    {
        //javabean 映射工具
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        MapperFacade mapper = mapperFactory.getMapperFacade();

        List<ObjectError> error=null;
        //返回boolean 是为了验证@Validated后面bean 里是否有不符合注解条件的错误信息
        if(bindingResult.hasErrors()){
            //获得所有错误信息返回list集合
            error=bindingResult.getAllErrors();
            for (ObjectError o:error) {
                //获得不符合要求的message
                return Result.failure(o.getDefaultMessage());
            }

        }
        if(!StringUtils.isEmpty(siteColumnBO.getTempindex())){
            ApiAssert.notTrue(!NumberUtils.isDigits(siteColumnBO.getTempindex()), "封面模板id参数错误");
        }
        if(!StringUtils.isEmpty(siteColumnBO.getTemplist())){
            ApiAssert.notTrue(!NumberUtils.isDigits(siteColumnBO.getTemplist()), "列表模板id参数错误");
        }
        if(!StringUtils.isEmpty(siteColumnBO.getTempcontent())){
            ApiAssert.notTrue(!NumberUtils.isDigits(siteColumnBO.getTempcontent()), "内容页面模板id参数错误");
        }
        if(!StringUtils.isEmpty(siteColumnBO.getTempalone())){
            ApiAssert.notTrue(!NumberUtils.isDigits(siteColumnBO.getTempalone()), "单独页面模板id参数错误");
        }
        SiteColumn column = mapper.map(siteColumnBO,SiteColumn.class);
        return siteColumnService.addUserSiteColumn(column);
    }

    @GetMapping("/columnform{url.suffix}")
    public Object columnForm(@RequestParam(value = "siteId", required = false) String siteId,Model model){
        ApiAssert.notTrue(siteId == null || !NumberUtils.isNumber(siteId), "网站id为空或者类型错误");
        //网站id
        model.addAttribute("siteId",siteId);
        return "system/admin/system/site/column/columnform";
    }

    @GetMapping("/columnTree{url.suffix}")
    @ResponseBody
    public Object columnTree(@RequestParam(value = "siteId", required = false) String siteId){
        ApiAssert.notTrue(siteId == null || !NumberUtils.isNumber(siteId), "网站id为空或者类型错误");
        Map<String,Object> map = new HashMap<>();
        Map<String,Object> status = new HashMap<>();
        status.put("code",200);
        status.put("message","操作成功");
        map.put("status",status);
        map.put("data",siteColumnService.findColumnTreeBySiteId(Long.parseLong(siteId)));
        return map;
    }

    /**
     * 网站分类列表
     *
     * @return
     */
    @GetMapping("/list{url.suffix}")
    public String siteList(@RequestParam(value = "siteId", defaultValue = "0") String siteId,
                           @RequestParam(value = "id", defaultValue = "0") String id, Model model)
    {
        ApiAssert.notTrue(siteId == null || !NumberUtils.isNumber(siteId), "网站id为空或者类型错误");
        model.addAttribute("siteId",siteId);
        return "system/admin/system/site/column/list";
    }

    @GetMapping("/listData{url.suffix}")
    @ResponseBody
    public Object siteList(@RequestParam(value = "siteId", defaultValue = "0") String siteId,
                           @RequestParam(value = "page",defaultValue = "1") Integer page,
                           @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize,
                           @Sort(accepts = {"add_time", "id"}) @RequestParam(defaultValue = "add_time") String sort,
                           @RequestParam(defaultValue = "desc") String order) {
        ApiAssert.notTrue(siteId == null || !NumberUtils.isNumber(siteId), "网站id为空或者类型错误");
        return LayResult.success(0,"true", 0, siteColumnService.selectColumnListTreetable(Long.parseLong(siteId)));
    }
}
